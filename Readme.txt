CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The field Collection Template module provides the Template formatter for the Field Collection module.

REQUIREMENTS
------------
This module requires Field Collection module.
http://www.drupal.org/project/field_collection

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
   for further information.

CONFIGURATION
-------------
 * Go to admin/structure/types/manage/CONTENT_TYPE/display
 * Switch the formatter to "Template for field collection items".

MAINTAINERS
-----------
Current maintainers:
 * Naveen Valecha (naveenvalecha) - https://drupal.org/u/naveenvalecha
